#include "cuda_runtime.h"
#include <cstdio>
#include <cmath>
#include <fstream>
#include <string>
#include <CertExit.h>

const float l = 3.1415;
const int N = 100;		// index t
const int M = 100;		// index x

const float tau = 0.5;		// step t
const float h = l / 10;		// step x
const float c = 0.1;		// phase velocity


#define gpuErrchk(ans) { gpuAssert((ans), __FILE__, __LINE__); }
inline void gpuAssert(cudaError_t code, const char *file, int line, bool abort = true)			// macro for check cuda error
{
	if (code != cudaSuccess)
	{
		fprintf(stderr, "GPUassert: %s %s %d\n", cudaGetErrorString(code), file, line);
		if (abort) exit(code);
	}
}


__global__
void init_zero(float *dev, size_t size, float h)						// init zeros layer
{
	int index = blockIdx.x * blockDim.x + threadIdx.x;

	if (index > 0 && index < size-1)
	{
		dev[index] = sinf(index * h);
	}
}

__global__
void init_first(float *dev, float *dev_0, size_t size, float h, float tau, float c)		// init first layer
{
	int index = blockIdx.x * blockDim.x + threadIdx.x;

	if (index < size-1 && index > 0)
	{
		dev[index] = dev_0[index] + tau * cosf(index * h) - 0.5 * pow(tau * c, 2) * sinf(index * h);
	}
}

__global__
void shift(float *dev_1, float *dev_2, float *dev_3, size_t size)			// shift data 
{
	int index = blockIdx.x * blockDim.x + threadIdx.x;

	if (index < size)
	{
		dev_1[index] = dev_2[index];
		dev_2[index] = dev_3[index];
		dev_3[index] = 0;
	}
}

__global__
void run(float *dev_yn, float *dev_yn_1, float *dev_yn_2, size_t size, float tau, float c, float h)		// main loop
{
	int index = blockIdx.x * blockDim.x + threadIdx.x;

	if (index > 0 && index < size - 1)
	{
		dev_yn_2[index] = 2. * dev_yn_1[index] - dev_yn[index] + pow(c*tau / h, 2) * (dev_yn_1[index + 1] - 2 * dev_yn_1[index] + dev_yn_1[index - 1]);
		if (dev_yn_2[index] > 4) dev_yn_2[index] = 4;
		if (dev_yn_2[index] < -4) dev_yn_2[index] = -4;
	}
}


void print(int t, float *host, std::ofstream& file)			// output in file stream
{	
	for (int i = 0; i < N; i++)
	{
		if (host[i] != 4)
		file << h * i << "," << t << "," << host[i] << "\n";
	}


}

void main()
{

	std::string m_path("D:/Projects_VS13/oscillation_equation/result/result.csv");
	std::ofstream fout(m_path, std::ios::app);

	fout << "x,y,z" << std::endl;

	float *host;

	float *dev_yn;
	float *dev_yn_1;
	float *dev_yn_2;

	host = new float[N];

	gpuErrchk(cudaMalloc((void**)&dev_yn, N * sizeof(float)));					// allocate cuda device memory
	gpuErrchk(cudaMalloc((void**)&dev_yn_1, N * sizeof(float)));
	gpuErrchk(cudaMalloc((void**)&dev_yn_2, N * sizeof(float)));

	memset(host, 0, N * sizeof(float));

	gpuErrchk(cudaMemcpy(dev_yn, host, N * sizeof(float), cudaMemcpyHostToDevice));		// init cuda device memory
	gpuErrchk(cudaMemcpy(dev_yn_1, host, N * sizeof(float), cudaMemcpyHostToDevice));
	gpuErrchk(cudaMemcpy(dev_yn_2, host, N * sizeof(float), cudaMemcpyHostToDevice));

	init_zero << < 2, 512 >> >(dev_yn, N, h);												// 0
	gpuErrchk(cudaMemcpy(host, dev_yn, N * sizeof(float), cudaMemcpyDeviceToHost));
	print(0, host, fout);


	init_first << < 2, 512 >> >(dev_yn_1, dev_yn, N, h, tau, c);							// 1
	gpuErrchk(cudaMemcpy(host, dev_yn_1, N * sizeof(float), cudaMemcpyDeviceToHost));
	print(tau, host, fout);

	for (int i = 2; i < M; i++)
	{
		run << < 2, 512 >> >(dev_yn, dev_yn_1, dev_yn_2, N, tau, c, h);							// main loop
		shift << < 2, 512 >> >(dev_yn, dev_yn_1, dev_yn_2, N);
		gpuErrchk(cudaMemcpy(host, dev_yn_1, N * sizeof(float), cudaMemcpyDeviceToHost));
		print(i*tau, host, fout);
	}

	fout.close();
}